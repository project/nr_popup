
Drupal.nr_popupOptionsCache = {}; /* collect values for quick access */

/**
 * Decorate and prepare given field(know by fieldId).
 */
Drupal.nr_popupAttach = function(fieldId, fieldName, nodeType, nodeId) {
  /* Get container */
  var field = $('#' + fieldId).parents('div:first');
  /* Move unselected to options cache*/
  Drupal.nr_popupOptionsCache[fieldId] = [];
  $('#' + fieldId + ' option').each(function(i, option) {
    Drupal.nr_popupOptionsCache[fieldId][$(option).val()*1] = $(option).text();
    if (!$(option).attr('selected')) {
      $('#' + fieldId).removeOption($(option).val());
    }
  });
  /* Insert popup with loader */
  field.append('<div id="' + fieldId + '_popup" class="flora"></div>');
  var _popup = $('#' + fieldId + '_popup');
  _popup.hide();
  /* Show loader */
  _popup.html(Drupal.nr_popupAjaxLoader);
  /* Insert button */
  field.append('<input type="button" value="' + Drupal.nr_popupButtonLabel + '" id="' + fieldId + '_button" />');
  $('#' + fieldId + '_button').click(function() {
    Drupal.nr_popupShow(_popup, fieldName, nodeType, nodeId, fieldId);
  });
};

/**
 * Prepare and show popup
 */
Drupal.nr_popupShow = function (_popup, fieldName, nodeType, nodeId, fieldId) {
  _popup.dialog({
    modal: true,
    overlay: {
      opacity: 0.5,
      background: 'black'
    },
    width: 550,
    minWidth: 350,
    height: 350,
    minHeight: 250,
    close: function(event, ui) {
      _popup.hide();
      _popup.html(Drupal.nr_popupAjaxLoader)
    }
  });
  /* Display popup */
  _popup.show();
  var url = Drupal.settings.basePath + '?q=nr_popup_js/' + fieldName + '/' + nodeType + '/' + nodeId;
  Drupal.nr_popupAjaxHandler(url, _popup, fieldId);
};

/**
 * Handle view ajax call on every load.
 */
Drupal.nr_popupAjaxHandler = function (url, _popup, fieldId) {
  _popup.html(Drupal.nr_popupAjaxLoader); /* show loader */
  $.get(url, function(data) {
    _popup.html(data);
    /* Header */
    $('.ui-dialog .view-cell-header a').click(function() {
      Drupal.nr_popupAjaxHandler($(this).attr('href'), _popup);
      return false;
    });
    /* Row links */
    $('.ui-dialog .view-field a').click(function() {
      window.open($(this).attr("href"));
      return false;
    });
    /* Pager */
    $('.ui-dialog .pager a').click(function() {
      Drupal.nr_popupAjaxHandler($(this).attr('href'), _popup, fieldId);
      return false;
    });
    $('.ui-dialog .view-content .view-field-node-nid').each(function(i, element) {
      if (i > 0) {
        var nodeId = $(element).text();
        $(element).html('<span></span>');
        if ($('#' + fieldId).containsOption(nodeId)) {
          Drupal.nr_popupAttachRemButton($('span', element), nodeId, fieldId);
        }
        else {
          Drupal.nr_popupAttachAddButton($('span', element), nodeId, fieldId);
        }
      }
    });
  });
};

/**
 * Attach add triggers to given object and decorates it.
 */
Drupal.nr_popupAttachAddButton = function (button, nodeId, fieldId) {
  $(button).attr({
    'class': '',
    'style': ''});
  $(button).click(function() {
    alert(nodeId);
    Drupal.nr_popupAddNode(this, nodeId, fieldId);
  });
};

/**
 * Attach remove triggers to given object and decorates it.
 */
Drupal.nr_popupAttachRemButton = function (button, nodeId, fieldId) {
  $(button).attr({
    'class': 'nr_popup-delete',
    'style': ''});
  $(button).click(function() {
    Drupal.nr_popupRemNode(this, nodeId, fieldId);
  });
};

/**
 * Add new node reference node to select box.
 */
Drupal.nr_popupAddNode = function (button, nodeId, fieldId) {
  /* Show throbber(ajax loader) */
  $(button).attr({
    'class': 'nr_popup-throbbing',
    'style': 'background-image: url(' + Drupal.settings.basePath + 'misc/throbber.gif)'});
  /* Add option */
  $('#' + fieldId).addOption(nodeId, Drupal.nr_popupOptionsCache[fieldId][nodeId*1]);
  /* Remove throbber and change icon */
  Drupal.nr_popupAttachRemButton(button, nodeId, fieldId);
};

/**
 * Remove referenced node from select box.
 */
Drupal.nr_popupRemNode = function (button, nodeId, fieldId) {
  /* Show throbber(ajax loader) */
  $(button).attr({
    'class': 'nr_popup-throbbing',
    'style': 'background-image: url(' + Drupal.settings.basePath + 'misc/throbber.gif)'});
  /* Add option */
  $('#' + fieldId).removeOption(nodeId);
  /* Remove throbber and change icon */
  Drupal.nr_popupAttachAddButton(button, nodeId, fieldId);
};
